Considerations
======================================================================

General considerations
----------------------------------------------------------------------
I've created the project with `cookiecutter-django <https://github.com/pydanny/cookiecutter-django>`_. The sheer number of things it includes by default may be overkill
for this exercise (e.g., production configs) but it is nonetheless really helpful as a template for starting almost any real project.

The API has been implemented with the help of `Django Rest Framework <https://www.django-rest-framework.org/>`_.

I've also included a description of the API by integrating `Yet another Swagger generator <https://github.com/axnsan12/drf-yasg>`_.

The easier way to **test the API** is working as expected is by:

* Checking the unit tests on the :code:`tests` folder of the :code:`users` and :code:`transactions` Django apps. The tests can be run with:
    ::

        docker exec belvo_backend_django pytest

* Playing with `Swagger UI <http://localhost:8000/swagger/>`_: it allows sending real requests. Note that you must log in first to Django admin before being able to use the endpoints.

This document will address the exercise step by step.

Exercise step-by-step: Purpose section
--------------------------------------

1. Can create users by receiving: name, email and age
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are two type of :ref:`users <users>` in my app: :code:`Users` and :code:`BankUsers`. The :code:`Users` are the operators of the API, the ones that can authenticate and use the endpoints. The :code:`BankUsers` are the ones referred as "users" in the exercise document: the ones that we create and we want to have an overview of their transactions.

I assumed that :code:`BankUsers` must be unique by e-mail and their max :code:`age` could be 120 (to prevent strange age inputs).

The way to create new users is by :code:`POST` to :code:`/api/bank-users/`.


2. List all users and also and see the details of a specific user
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
There is not much to comment on here:

* You can obtain the list of users via :code:`GET` to :code:`/api/bank-users/`
* You can get the detail of a user via :code:`GET` to :code:`/api/bank-users/{id}/`


3. Can save users' transactions. Each transaction has: reference (unique), account, date, amount, type and category
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I created the :code:`Transaction` model in a different Django app although they are so closely related that it might make more sense to have them on the same Django app (it would depend on how big would their objects grow).

You can create transactions by :code:`POST` ing to :code:`/api/transactions/`.

A typical discussion regarding the :ref:`transactions` model is what to do with the transactions of a user if that user is deleted. In this case, I opted by not allowing to delete a user with transactions, thinking that we would implement some sort of soft-deletion of the user, not involving a real deletion of it.

Let's have a look at the constraints.

A transaction reference is unique.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Simply enforced with :code:`unique=True` in the Django model. By looking at the `Goals` section of the problem statement document, it seems that if another transaction appears with the same :code:`reference` we must simply ignore it (and not update it).

There are only two types of transactions: inflow and outflow.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This is enforced by using :code:`choices` in the type field of the transaction model. In this case, we are simply specifying the values in the same format they will arrive through the API, that is, :code:`inflow` and :code:`outflow`. This allows us to make no transformation of values when serializing/deserializing, and makes DB dumps easier to understand, but there could be performance costs.

All outflow/inflow transactions amounts are negative/positive decimal numbers.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To make sure this is enforced, we must add an additional validation to the serializer (see :ref:`transaction_serializers`), but we must also do it in the save method of Transaction (see :ref:`transactions`).

We expect to receive transactions in bulk as well.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
I override :code:`get_serializer` so that I can instantiate the serializer with :code:`many=True` when we receive a list. See `transactions/views.py <https://bitbucket.org/mario_lopeztelecom/belvo_backend/src/5352e23dc7ba92b3d34ebf3506e66099ff657ac8/belvo_backend/transactions/views.py#views.py-14>`_.

Additionally, I needed to use a custom list serializer to be able to:

* Ignore transactions that cannot be validated in the bulk create (instead of preventing the whole bulk creation, which is the default behavior).
* Make sure there are no reference duplicates within the bulk create.

See :ref:`transaction_serializers`.

The transactions we receive could be already in our system, thus we need to avoid duplicating them in our database
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Already described in the above paragraphs.


Exercise step-by-step: Goals
----------------------------

1. Given an user id, we want to be able to see a summary by account that shows the balance of the account, total inflow and total outflows. It should be possible to specify a date range, if no date range is given all transactions should be considered.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I provide this functionality through :code:`GET` to :code:`/api/bank-users/{id}/summary_by_account/?date_start='YYYY-mm-dd'&date_end='YYYY-mm-dd'` where :code:`date_start` and :code:`date_end` are both optional.

Fortunately, Django ORM is really powerful regarding annotations. See `users/views.py <https://bitbucket.org/mario_lopeztelecom/belvo_backend/src/5352e23dc7ba92b3d34ebf3506e66099ff657ac8/belvo_backend/users/views.py#views.py-20>`_.

2. We want to be able to see a user's summary by category that shows the sum of amounts per transaction category.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
I provide this functionality through :code:`GET` to :code:`/api/bank-users/{id}/summary_by_account/`.

It is an easier query than the other one but requires us to manipulate the result a bit more to express it in the expected format.
