.. belvo-backend documentation master file, created by sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mario L. Belvo Backend (API) Case Challenge documentation
======================================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   considerations
   users
   transactions
   transaction_serializers


.. toctree::
   :maxdepth: 3
   :caption: Docs already included by cookiecutter-django:

   howto
   pycharm/configuration



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
