from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from belvo_backend.transactions.views import TransactionViewSet
from belvo_backend.users.views import BankUserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("bank-users", BankUserViewSet, basename="bank_user")
router.register("transactions", TransactionViewSet, basename="transaction")


app_name = "api"
urlpatterns = router.urls
