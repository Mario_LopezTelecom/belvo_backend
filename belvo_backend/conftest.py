import pytest

from belvo_backend.users.tests.factories import UserFactory


@pytest.fixture
def auth_client(client):
    user_pass = "a_password"
    user = UserFactory(password=user_pass)
    client.login(username=user.username, password=user_pass)
    return client
