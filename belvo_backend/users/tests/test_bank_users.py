import json
import os

import pytest
from rest_framework.status import is_success

from belvo_backend.users.models import BankUser
from belvo_backend.users.serializers import BankUserSerializer
from belvo_backend.users.tests.factories import BankUserFactory

pytestmark = pytest.mark.django_db


class TestBankUserViewSet:
    def test_create_bank_user(self, auth_client):
        """Simple check of a regular case of creating a bank user.
        """
        list_url = "/api/bank-users/"
        create_data = {
            "name": "Jane Doe",
            "email": "jane@email.com",
            "age": 23,
        }
        response = auth_client.post(
            list_url, create_data, content_type="application/json"
        )
        assert is_success(response.status_code)
        bank_users = BankUser.objects.all()
        assert bank_users.count() == 1
        bank_user = bank_users.first()
        for key in create_data:
            assert getattr(bank_user, key) == create_data[key]

    def test_retrieve_bank_user(self, auth_client):
        """Simple retrieval of a bank user
        """
        bank_user = BankUserFactory()
        detail_url = f"/api/bank-users/{bank_user.pk}/"
        response = auth_client.get(detail_url)
        assert is_success(response.status_code)
        response_dict = response.json()
        for field in BankUserSerializer.Meta.fields:
            assert getattr(bank_user, field) == response_dict.get(field)

    def test_list_bank_users(self, auth_client):
        """Simple retrieval of list of bank users
        """
        bank_users = BankUserFactory.create_batch(5)
        list_url = "/api/bank-users/"
        response = auth_client.get(list_url)
        assert is_success(response.status_code)
        response_dict = {user_obj["email"]: user_obj for user_obj in response.json()}
        assert len(response_dict) == len(bank_users)
        for bank_user in bank_users:
            bank_user_response = response_dict[bank_user.email]
            for field in BankUserSerializer.Meta.fields:
                assert getattr(bank_user, field) == bank_user_response.get(field)

    @pytest.mark.parametrize(
        "summary_url, expected_response",
        [
            (
                "/api/bank-users/1/summary_by_account/",
                [
                    {
                        "account": "C00099",
                        "balance": "1738.87",
                        "total_inflow": "2500.72",
                        "total_outflow": "-761.85",
                    },
                    {
                        "account": "S00012",
                        "balance": "150.72",
                        "total_inflow": "150.72",
                        "total_outflow": "0.00",
                    },
                ],
            ),
            (
                "/api/bank-users/1/summary_by_account/?date_start=2020-01-05",
                [
                    {
                        "account": "C00099",
                        "balance": "1790.00",
                        "total_inflow": "2500.72",
                        "total_outflow": "-710.72",
                    },
                    {
                        "account": "S00012",
                        "balance": "150.72",
                        "total_inflow": "150.72",
                        "total_outflow": "0.00",
                    },
                ],
            ),
            (
                "/api/bank-users/1/summary_by_account/?date_start=2020-01-05&date_end=2020-01-12",
                [
                    {
                        "account": "C00099",
                        "balance": "2350.00",
                        "total_inflow": "2500.72",
                        "total_outflow": "-150.72",
                    },
                    {
                        "account": "S00012",
                        "balance": "150.72",
                        "total_inflow": "150.72",
                        "total_outflow": "0.00",
                    },
                ],
            ),
        ],
    )
    def test_summary_by_account(self, auth_client, summary_url, expected_response):
        BankUserFactory(id=1)
        dirname = os.path.dirname(__file__)
        with open(os.path.join(dirname, "test_transactions.json")) as f:
            bulk_transactions = json.load(f)
        list_url = "/api/transactions/"
        auth_client.post(list_url, bulk_transactions, content_type="application/json")
        response = auth_client.get(summary_url)
        response_json = response.json()
        assert response_json == expected_response

    def test_summary_by_type_and_category(self, auth_client):
        BankUserFactory(id=1)
        dirname = os.path.dirname(__file__)
        with open(os.path.join(dirname, "test_transactions.json")) as f:
            bulk_transactions = json.load(f)
        list_url = "/api/transactions/"
        auth_client.post(list_url, bulk_transactions, content_type="application/json")
        summary_url = "/api/bank-users/1/summary_by_type_and_category/"
        response = auth_client.get(summary_url)
        response_json = response.json()
        expected_response = {
            "inflow": {"salary": "2500.72", "savings": "150.72"},
            "outflow": {
                "groceries": "-51.13",
                "rent": "-560.00",
                "transfer": "-150.72",
            },
        }
        assert response_json == expected_response
