from rest_framework import serializers

from belvo_backend.users.models import BankUser


class BankUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankUser
        fields = ["name", "email", "age"]
