from django.db.models import Case, F, Sum, Value, When
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from belvo_backend.transactions.models import Transaction
from belvo_backend.users.models import BankUser
from belvo_backend.users.serializers import BankUserSerializer


class BankUserViewSet(
    CreateModelMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet
):
    serializer_class = BankUserSerializer
    queryset = BankUser.objects.all()

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "date_start",
                openapi.IN_QUERY,
                required=False,
                type=openapi.TYPE_STRING,
                format=openapi.FORMAT_DATE,
            ),
            openapi.Parameter(
                "date_end",
                openapi.IN_QUERY,
                required=False,
                type=openapi.TYPE_STRING,
                format=openapi.FORMAT_DATE,
            )
        ]
    )
    @action(detail=True, methods=["get"])
    def summary_by_account(self, request, pk):
        """Given an user id, we want to be able to see a summary by account that shows the
        balance of the account, total inflow and total outflows. It should be possible to specify a
        date range (with queryparams :code:`date_start` and :code:`date_end`), if no date range is given all
        transactions should be considered.
        """
        bank_user = get_object_or_404(BankUser, pk=pk)
        transactions_to_consider = bank_user.transaction_set
        date_start = request.query_params.get("date_start")
        date_end = request.query_params.get("date_end")
        if date_start:
            transactions_to_consider = transactions_to_consider.filter(
                date__gte=date_start
            )
        if date_end:
            transactions_to_consider = transactions_to_consider.filter(
                date__lte=date_end
            )
        summary = transactions_to_consider.values("account").annotate(
            balance=Sum("amount"),
            total_inflow=Sum(
                Case(
                    When(type=Transaction.Type.INFLOW, then=F("amount")),
                    default=Value("0.00"),
                )
            ),
            total_outflow=Sum(
                Case(
                    When(type=Transaction.Type.OUTFLOW, then=F("amount")),
                    default=Value("0.00"),
                )
            ),
        )
        # safe = False to allow non-dicts to be JSON serialized.
        return JsonResponse(list(summary), safe=False)

    @action(detail=True, methods=["get"])
    def summary_by_type_and_category(self, request, pk):
        """We want to be able to see a user's summary by category that shows the sum of amounts per transaction category
        """
        bank_user = get_object_or_404(BankUser, pk=pk)
        summary = bank_user.transaction_set.values("type", "category").annotate(
            Sum("amount")
        )
        # Adapt the result format to the expected one.
        result = {"inflow": {}, "outflow": {}}
        for elem in summary:
            result[elem["type"]][elem["category"]] = elem["amount__sum"]
        return JsonResponse(result)
