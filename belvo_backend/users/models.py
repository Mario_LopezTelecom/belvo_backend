from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator
from django.db.models import CharField, EmailField, Model, PositiveIntegerField
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """Default user for belvo-backend.

    This is the user of the API, as opposed to the bank users we are managing with the API.
    """

    pass


class BankUser(Model):
    """These are the bank users, whose transaction info we are going to manage through this API.
    """

    name = CharField(help_text=_("The name of the bank user"), max_length=254)
    email = EmailField(help_text=_("Contact e-mail. Must be unique"), unique=True)
    age = PositiveIntegerField(
        help_text=_("Age of the bank user"),
        validators=[MaxValueValidator(120)]
    )
