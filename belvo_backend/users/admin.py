from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from belvo_backend.users.models import BankUser

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    list_display = ["username", "is_superuser"]
    search_fields = ["username"]


@admin.register(BankUser)
class BankUserAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "age"]
    search_fields = ["name", "email"]
