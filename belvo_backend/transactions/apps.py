from django.apps import AppConfig


class TransactionsConfig(AppConfig):
    name = "belvo_backend.transactions"
