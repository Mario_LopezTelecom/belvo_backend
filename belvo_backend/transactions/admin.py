from django.contrib import admin

from belvo_backend.transactions.models import Transaction


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = [
        "reference",
        "account",
        "date",
        "amount",
        "type",
        "category",
        "user",
    ]
    search_fields = ["reference"]
