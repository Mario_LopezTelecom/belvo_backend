from django.core.exceptions import ValidationError
from django.db.models import (
    PROTECT,
    CharField,
    DateField,
    DecimalField,
    ForeignKey,
    Model,
    TextChoices,
)
from django.utils.translation import gettext_lazy as _

from belvo_backend.users.models import BankUser


class Transaction(Model):
    """These are the bank users, whose transaction info we are going to manage through this API.
    """

    reference = CharField(
        help_text=_("Unique identifier of the transaction"), unique=True, max_length=10
    )

    # I imagine that in a real case, this probably be a Foreign Key to an Account model.
    account = CharField(
        help_text=_("Account id the transaction was made from/to"), max_length=10
    )

    date = DateField(help_text=_("Date of the transaction"))
    amount = DecimalField(
        help_text=_(
            "Amount of the transaction: positive if inflow, negative if outflow"
        ),
        max_digits=15,
        decimal_places=2,
    )

    # It should be discussed whether it makes sense to store this as a shortened representation or even integers
    # for sapce and performance considerations, but for now this is easier to understand (and the dumps of the DB too).
    class Type(TextChoices):
        INFLOW = "inflow", _("Inflow")
        OUTFLOW = "outflow", _("Outflow")

    type = CharField(
        help_text=_("A transaction can be inflow or outflow"),
        choices=Type.choices,
        max_length=7,
    )

    # I will consider here a CharField but it could probably be a Foreign Key to a Category model.
    category = CharField(
        help_text=_("Whether it is grocery expense, a transfer..."), max_length=254
    )

    # I assume we wouldn't want to delete the transactions when we delete the user. There are
    # several things that can be done here, for example, a soft delete of the user (we remove private info)
    # keeping the id for analytic purposes.
    user = ForeignKey(
        BankUser, help_text=_("Bank user that made the transaction"), on_delete=PROTECT
    )

    def clean(self):
        """
        We override this method to provide multi field validation in this model.
        """
        super().clean()
        self.validate_type_and_amount(self.type, self.amount)

    @staticmethod
    def validate_type_and_amount(transaction_type, amount):
        if transaction_type == Transaction.Type.INFLOW and amount < 0:
            raise ValidationError(_("An inflow transaction cannot be negative"))
        if transaction_type == Transaction.Type.OUTFLOW and amount > 0:
            raise ValidationError(_("An outflow transaction cannot be positive"))

    def save(self, *args, **kwargs):
        """
        Although this is a bit controversial in Django community, this is the only
        way to force validation on model save, although it creates Server Error if not handled.
        In any case, this is not called upon bulk_create or update methods, thus, we need to validate
        in some other places too.
        """
        self.full_clean()
        return super().save(*args, **kwargs)
