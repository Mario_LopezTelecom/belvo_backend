from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.relations import PrimaryKeyRelatedField

from belvo_backend.transactions.models import Transaction
from belvo_backend.users.models import BankUser


class TransactionListSerializer(serializers.ListSerializer):
    """Custom ListSerializer to ignore objects with validation erros.

    We need to define a custom ListSerializer to override the :code:`to_internal_value` method so that we do not trigger
    a :code:`ValidationError` simply because one of the transactions does not pass validation.

    We also check that :code:`reference` is not repeated in the list.
    """
    def to_internal_value(self, data):
        ret = []
        errors = []
        references = set()

        for item in data:
            try:
                validated = self.child.run_validation(item)
            except ValidationError as exc:
                errors.append(exc.detail)
            else:
                if validated["reference"] not in references:
                    ret.append(validated)
                    references.add(validated["reference"])
                    errors.append({})
        return ret


class TransactionSerializer(serializers.ModelSerializer):
    user_id = PrimaryKeyRelatedField(queryset=BankUser.objects.all(), source="user")

    def validate(self, data):
        """
        In reality, there is no need to call super, since this method is called in addition to regular validation
        but it might be confusing: somebody could think that regular validation is being overridden.
        """
        Transaction.validate_type_and_amount(data["type"], data["amount"])
        return super().validate(data)

    class Meta:
        model = Transaction
        fields = [
            "reference",
            "account",
            "date",
            "amount",
            "type",
            "category",
            "user_id",
        ]
        list_serializer_class = TransactionListSerializer
