from rest_framework.mixins import CreateModelMixin
from rest_framework.viewsets import GenericViewSet

from belvo_backend.transactions.models import Transaction
from belvo_backend.transactions.serializers import TransactionSerializer


class TransactionViewSet(
    CreateModelMixin, GenericViewSet
):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()

    def get_serializer(self, *args, **kwargs):
        """
        We need to override get_serializer if we want to post a list of transactions.
        """
        if "data" in kwargs:
            data = kwargs["data"]

            # Check if many is required
            if isinstance(data, list):
                kwargs["many"] = True

        return super().get_serializer(*args, **kwargs)
