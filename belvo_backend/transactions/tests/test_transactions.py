import datetime
import json
import os
from decimal import Decimal

import pytest
from django.core.exceptions import ValidationError
from rest_framework.status import is_client_error, is_success

from belvo_backend.transactions.models import Transaction
from belvo_backend.transactions.tests.factories import TransactionFactory
from belvo_backend.users.tests.factories import BankUserFactory

pytestmark = pytest.mark.django_db


class TestTransactions:
    def test_cannot_create_positive_outflow_transaction_outside_API(self):
        with pytest.raises(ValidationError) as excinfo:
            TransactionFactory(amount=Decimal("12.23"), type=Transaction.Type.OUTFLOW)
        assert "outflow transaction cannot be positive" in str(excinfo.value)

    def test_cannot_create_negative_inflow_transaction_outside_API(self):
        with pytest.raises(ValidationError) as excinfo:
            TransactionFactory(amount=Decimal("-12.23"), type=Transaction.Type.INFLOW)
        assert "inflow transaction cannot be negative" in str(excinfo.value)

    def test_create_transaction(self, auth_client):
        bank_user = BankUserFactory()
        list_url = "/api/transactions/"
        create_data = {
            "reference": "000051",
            "account": "S00099",
            "date": "2020-01-13",
            "amount": "-51.13",
            "type": "outflow",
            "category": "groceries",
            "user_id": bank_user.id,
        }
        response = auth_client.post(
            list_url, create_data, content_type="application/json"
        )
        assert is_success(response.status_code)
        transactions = Transaction.objects.all()
        assert transactions.count() == 1
        transaction = transactions.first()
        assert transaction.date == datetime.date(2020, 1, 13)
        assert transaction.amount == Decimal(create_data["amount"])
        assert transaction.user == bank_user

    def test_cannot_create_positive_outflow_transaction(self, auth_client):
        bank_user = BankUserFactory()
        list_url = "/api/transactions/"
        create_data = {
            "reference": "000051",
            "account": "S00099",
            "date": "2020-01-13",
            "amount": "51.13",
            "type": "outflow",
            "category": "groceries",
            "user_id": bank_user.id,
        }
        response = auth_client.post(
            list_url, create_data, content_type="application/json"
        )
        assert is_client_error(response.status_code)
        assert "An outflow transaction cannot be positive" in str(response.json())
        transactions = Transaction.objects.all()
        assert transactions.count() == 0

    def test_cannot_create_negative_inflow_transaction(self, auth_client):
        bank_user = BankUserFactory()
        list_url = "/api/transactions/"
        create_data = {
            "reference": "000051",
            "account": "S00099",
            "date": "2020-01-13",
            "amount": "-51.13",
            "type": "inflow",
            "category": "groceries",
            "user_id": bank_user.id,
        }
        response = auth_client.post(
            list_url, create_data, content_type="application/json"
        )
        assert is_client_error(response.status_code)
        assert "An inflow transaction cannot be negative" in str(response.json())
        transactions = Transaction.objects.all()
        assert transactions.count() == 0

    def test_create_bulk_transaction(self, auth_client):
        BankUserFactory(id=1)
        dirname = os.path.dirname(__file__)
        with open(os.path.join(dirname, "test_transactions.json")) as f:
            bulk_transactions = json.load(f)
        list_url = "/api/transactions/"
        response = auth_client.post(
            list_url, bulk_transactions, content_type="application/json"
        )
        assert is_success(response.status_code)
        transactions = Transaction.objects.all()
        assert transactions.count() == 5
