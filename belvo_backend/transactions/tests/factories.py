import datetime

import pytz
from factory import Faker, LazyAttribute, LazyFunction, SubFactory, fuzzy
from factory.django import DjangoModelFactory

from belvo_backend.transactions.models import Transaction
from belvo_backend.users.tests.factories import BankUserFactory


def get_random_date():
    """
    Returns a random datetime
    """
    return Faker(
        "date_time_between_dates",
        datetime_start=datetime.datetime.now() - datetime.timedelta(days=15),
        datetime_end=datetime.datetime.now(),
        tzinfo=pytz.utc,
    ).generate()


class TransactionFactory(DjangoModelFactory):
    reference = Faker("bothify", text="#####????")
    account = Faker("bothify", text="#####????")
    date = LazyFunction(get_random_date)
    type = fuzzy.FuzzyChoice(Transaction.Type.values)
    amount = LazyAttribute(
        lambda transaction: fuzzy.FuzzyDecimal(low=0, high=1_000_000)
        if transaction.type == Transaction.Type.INFLOW
        else fuzzy.FuzzyDecimal(low=1_000_000, high=0)
    )
    category = fuzzy.FuzzyChoice(
        ["groceries", "salary", "transfer", "rent", "savings" "other"]
    )
    user = SubFactory(BankUserFactory)

    class Meta:
        model = Transaction
