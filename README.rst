belvo-backend
=============

Belvo Backend (API) Case Challenge

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


Instructions on running the app
-------------------------------
This repo implements a Django app working on PostgreSQL, both dockerized. Assuming you have already installed Docker
and Docker Compose, just run on your terminal

    ::

        docker-compose -f local.yml up --build -d

For convenience, I also offer a Makefile to run common management commands.

The endpoints provided by the app require to be authenticated, thus, you will need to create a user. The superuser
account can be used for this purpose. To create an superuser account, use this command

    ::

        docker exec -it belvo_backend_django python manage.py createsuperuser

The endpoints are documented (and can be tested) by accessing http://localhost:8000/swagger/

There is also a more detailed description of the exercise in http://localhost:7000/
